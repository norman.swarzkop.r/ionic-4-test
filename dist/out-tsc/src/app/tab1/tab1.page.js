import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Events, NavController } from '@ionic/angular';
//import { AES256 } from '@ionic-native/aes-256';
import { ToastController, AlertController } from '@ionic/angular';
import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage';
var Tab1Page = /** @class */ (function () {
    function Tab1Page(platform, toastCtrl, events, navCtrl, storage, alertCtrl) {
        this.platform = platform;
        this.toastCtrl = toastCtrl;
        this.events = events;
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.alertCtrl = alertCtrl;
        this.pPit = '';
        this.pSeam = '';
        this.pKodedriver = '';
        this.pDriver = '';
        this.pNobody = '';
        this.pNopol = '';
        this.pKontraktor = '';
        this.pChecker = '';
        this.Tiket = 0;
        this.trig = false;
        this.secureKey = '1234567890abcdefghijklmnopqrstuv'; // Any string, the length should be 32
        this.secureIV = '1234567890abcdef'; // Any string, the length should be 16
        this.PIT = ['001', '002', '003', '004', '00'];
        //this.generateSecureKeyAndIV(); 
        this.getFormattedDate();
        // IcYkw92J4cXKWMiViMI9sA==
        // To generate random secure key
        //this.generateSecureKey('1234');  // Optional
        // To generate random secure IV
        //this.generateSecureIV('1234');   // Optional
        //let data = "test";
        //this.encrypt(this.secureKey, this.secureIV, data); 
        //let encryptedData = "AE#3223==";
        //this.decrypt(this.secureKey, this.secureIV, encryptedData);  
        //this.pNopol='masuk pak';
    }
    Tab1Page.prototype.createUser = function () {
        this.rawPrint();
        this.trig = true;
        this.events.publish('hello', this.rawData, this.trig);
        /*this.events.subscribe('hello', (this.sendData())=> {
          // user and time are the same arguments passed in `events.publish(user, time)`
          this.dataSend= panel11+" "+ time;
        });*/
    };
    Tab1Page.prototype.rawPrint = function () {
        this.getFormattedDate();
        this.Tiket = this.Tiket + 1;
        this.rawData = '\x1B\x64\x01\x1B\x40\x1B\x74\x00\x1B\x61\x01\x1B\x21\x18DO Pengiriman Batubara\n';
        this.rawData += 'PT BSPC\n\n';
        this.rawData += '\x1B\x61\x00\x1B\x21\x08No tiket   : ' + this.Tiket + '\n';
        this.rawData += '\x1B\x61\x00\x1B\x21\x00PIT        : ' + this.pPit + '\n';
        this.rawData += 'SEAM       : ' + this.pSeam + '\n';
        this.rawData += '\x1B\x61\x00\x1B\x21\x08Kode Driver: ' + this.pKodedriver + '\n';
        this.rawData += 'Driver     : ' + this.pDriver + '\n';
        this.rawData += 'No Body    : ' + this.pNobody + '\n';
        this.rawData += '\x1B\x61\x00\x1B\x21\x00No Pol     : ' + this.pNopol + '\n';
        this.rawData += 'Kontraktor : ' + this.pKontraktor + '\n';
        this.rawData += 'Tgl        : ' + this.formattedDate + '\n';
        this.rawData += 'Jam        : ' + this.Time + '\n\n\n\n\n';
        this.rawData += 'Checker    : ' + this.pChecker + '\n\n';
        //this.rawData +='QR code :';
        if (this.checkQR == true) {
            var text = this.Tiket + ',' + this.pPit + ',' + this.pSeam + ',' + this.pKodedriver + ',' + this.pNobody + ',' + this.pKontraktor + ',' + this.fDate + ',' + this.Time;
            //this.dataEncrypt=text;
            var dataq = {};
            var len = text.length + 3;
            dataq = ['\x00', '\x01', '\x02', '\x03', '\x04', '\x05', '\x06', '\x07', '\x08', '\x09', '\x0A', '\x0B', '\x0C', '\x0D', '\x0E', '\x0F', '\x10', '\x11', '\x12', '\x13', '\x14',
                '\x15', '\x16', '\x17', '\x18', '\x19', '\x1A', '\x1B', '\x1C', '\x1D', '\x1E', '\x1F', '\x20', '\x21', '\x22', '\x23', '\x24', '\x25', '\x26', '\x27', '\x28', '\x29',
                '\x2A', '\x2B', '\x2C', '\x2D', '\x2E', '\x2F', '\x30', '\x31', '\x32', '\x33', '\x34', '\x35', '\x36', '\x37', '\x38', '\x39', '\x3A', '\x3B', '\x3C', '\x3D', '\x3E',
                '\x3F', '\x40', '\x41', '\x42', '\x43', '\x44', '\x45', '\x46', '\x47', '\x48', '\x49', '\x4A', '\x4B', '\x4C', '\x4D', '\x4E', '\x4F', '\x50', '\x51', '\x52', '\x53',
                '\x54', '\x55', '\x56', '\x57', '\x58', '\x59', '\x5A', '\x5B', '\x5C', '\x5D', '\x5E', '\x5F', '\x60', '\x61', '\x62', '\x63', '\x64', '\x65', '\x66', '\x67', '\x68',
                '\x69', '\x6A', '\x6B', '\x6C', '\x6D', '\x6E', '\x6F', '\x70', '\x71', '\x72', '\x73', '\x74', '\x75', '\x76', '\x77', '\x78', '\x79', '\x7A', '\x7B', '\x7C', '\x7D',
                '\x7E', '\x7F', '\x80', '\x81', '\x82', '\x83', '\x84', '\x85', '\x86', '\x87', '\x88', '\x89', '\x8A', '\x8B', '\x8C', '\x8D', '\x8E', '\x8F', '\x90', '\x91', '\x92'];
            this.rawData += '\x1B\x40\x1B\x74\x00\x1B\x40\x1B\x61\x01\x1D\x28\x6B\x04\x00\x31\x41\x32\x00\x1D\x28\x6B\x03\x00\x31\x43\x08\x1D\x28\x6B\x03\x00\x31\x45\x33\x1D\x28\x6B' + dataq[len] + '\x00\x31\x50\x30' + text + '\x1D\x28\x6B\x03\x00\x31\x51\x30\x0D\x0D';
            this.rawData += '\x1B\x61\x01\x1B\x21\x00' + this.Tiket + ' ';
            this.rawData += this.formattedDate + ' ';
            this.rawData += this.Time + '\n\n\n\n\n';
        }
        else {
            this.rawData += '\n\n\n';
        }
    };
    Tab1Page.prototype.getFormattedDate = function () {
        var dataObj = new Date();
        var year = dataObj.getFullYear().toString();
        var month = dataObj.getMonth().toString();
        var date = dataObj.getDate().toString();
        var hours = dataObj.getHours().toString();
        var minute = dataObj.getMinutes().toString();
        var second = dataObj.getSeconds().toString();
        var montArray = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
        var minArray = ['00', '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31', '32', '33', '34', '35', '36', '37', '38', '39', '40', '41', '42', '43', '44', '45', '46', '47', '48', '49', '50', '51', '52', '53', '54', '55', '56', '57', '58', '59'];
        var secArray = ['00', '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31', '32', '33', '34', '35', '36', '37', '38', '39', '40', '41', '42', '43', '44', '45', '46', '47', '48', '49', '50', '51', '52', '53', '54', '55', '56', '57', '58', '59'];
        var mArray = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
        this.formattedDate = date + ' ' + montArray[month] + ' ' + year;
        this.fDate = date + '-' + mArray[month] + '-' + year;
        this.Time = hours + ':' + minArray[minute] + ':' + secArray[second];
    };
    /*
      async generateSecureKeyAndIV()
      {
        let key='tigaresi56';
        this.secureKey = await this.aes256.generateSecureKey(key); // Returns a 32 bytes string
        this.secureIV = await this.aes256.generateSecureIV(key); // Returns a 16 bytes string
    
        this.aes256.encrypt(this.secureKey, this.secureIV, this.pEncrypt)
        .then(res => this.pEncrypt = res)
        .catch((error: any) => this.showToast(error));
      }
     
      decrypt()
      {
        this.aes256.decrypt(this.secureKey, this.secureIV, this.pEncrypt )
        .then(res => this.pDecrypt = res)
        .catch((error: any) => this.showToast(error));
      }*/
    Tab1Page.prototype.encrypt1 = function () {
        this.encrypt(this.secureKey, this.secureIV, this.pEncrypt);
    };
    Tab1Page.prototype.encrypt = function (secureKey, secureIV, data) {
        var _this = this;
        this.platform.ready().then(function () {
            cordova.plugins.AES256.encrypt(secureKey, secureIV, data, function (encrypedData) {
                //this.pNobody='masuk pak';
                _this.pEncrypt = encrypedData;
                _this.showToast('Ok');
            }, function (error) {
                _this.showToast(error);
            });
        });
    };
    Tab1Page.prototype.decrypt1 = function () {
        this.decrypt(this.secureKey, this.secureIV, this.pDecrypt);
    };
    Tab1Page.prototype.decrypt = function (secureKey, secureIV, encryptedData) {
        var _this = this;
        this.platform.ready().then(function () {
            cordova.plugins.AES256.decrypt(secureKey, secureIV, encryptedData, function (decryptedData) {
                //this.pDriver='masuk pak';
                _this.pDecrypt = decryptedData;
                _this.showToast('Ok');
            }, function (error) {
                _this.showToast(error);
            });
        });
    };
    Tab1Page.prototype.generateSecureKey = function (password) {
        var _this = this;
        this.platform.ready().then(function () {
            cordova.plugins.AES256.generateSecureKey(password, function (secureKey) {
                _this.secureKey = secureKey;
                //this.showToast(secureKey);  
                //this.pTiket=secureKey +' '+password;        
            }, function (error) {
                _this.showToast(error);
            });
        });
    };
    Tab1Page.prototype.generateSecureIV = function (password) {
        var _this = this;
        this.platform.ready().then(function () {
            cordova.plugins.AES256.generateSecureIV(password, function (secureIV) {
                _this.secureIV = secureIV;
                //this.showToast(secureIV);     
                _this.pPit = secureIV + ' ' + password;
            }, function (error) {
                _this.showToast(error);
            });
        });
    };
    Tab1Page.prototype.showToast = function (msj) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var toast;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastCtrl.create({
                            message: msj,
                            position: 'bottom',
                            duration: 1000
                        })];
                    case 1:
                        toast = _a.sent();
                        return [4 /*yield*/, toast.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    Tab1Page.prototype.showError = function (error) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var alert;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            header: 'Error',
                            //subHeader: 'error',
                            message: error,
                            buttons: ['Dismiss']
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    Tab1Page.prototype.man = function () {
        this.getValue(this.pMan);
    };
    Tab1Page.prototype.test = function () {
        let;
        this.data = {};
        //Set String Value
        this.setValue("name", "Freaky Jolly");
        //Set Integer Value
        this.setValue("phone", 8908904);
        var sampleObj = [
            {
                country: "United States of America",
                address: "638 Fahey Overpass",
                phone: "257.255.3201"
            }, {
                country: "United States of America",
                address: "141 Schowalter Mount",
                phone: "1-868-497-5043 x73289"
            }, {
                country: "United States of America",
                address: "025 Schultz Via",
                phone: "1-814-823-5520 x68656"
            }
        ];
        //Set Object Value
        this.setValue("offices", sampleObj);
    };
    // set a key/value
    Tab1Page.prototype.setValue = function (key, value) {
        var _this = this;
        this.storage.set(key, value).then(function (response) {
            _this.showToast('set' + key + ' ' + response);
            //get Value Saved in key
            //this.getValue(key);
        }).catch(function (error) {
            _this.showToast('set error for ' + key + ' ' + error);
        });
    };
    // get a key/value pair
    Tab1Page.prototype.getValue = function (key) {
        var _this = this;
        this.storage.get(key).then(function (val) {
            _this.data[key] = "";
            _this.data[key] = val;
            _this.showToast('get  ' + key + ' ' + val);
            _this.pMan = val;
        }).catch(function (error) {
            _this.showToast('get error for ' + key + ' ' + error);
        });
    };
    // Remove a key/value pair
    Tab1Page.prototype.removeKey = function (key) {
        var _this = this;
        this.storage.remove(key).then(function () {
            _this.showToast('removed ' + key);
            _this.data[key] = "";
        }).catch(function (error) {
            _this.showToast('removed error for ' + key + '' + error);
        });
    };
    //Get Current Storage Engine Used
    Tab1Page.prototype.driverUsed = function () {
        this.showToast("Driver Used: " + this.storage.driver);
    };
    // Traverse key/value pairs
    Tab1Page.prototype.traverseKeys = function () {
        var _this = this;
        this.storage.forEach(function (value, key, iterationNumber) {
            _this.pTest = "key=" + key + " iterationNumber=" + iterationNumber + " value=" + value;
        });
    };
    // Traverse key/value pairs
    Tab1Page.prototype.listKeys = function () {
        var _this = this;
        this.storage.keys().then(function (k) {
            _this.showToast(k[0] + '\n' + k[1] + '\n' + k[2] + '\n' + k[3] + '\n' + k[4]);
        });
    };
    // Total Keys Stored
    Tab1Page.prototype.getKeyLength = function () {
        var _this = this;
        this.storage.length().then(function (keysLength) {
            _this.showToast("Total Keys " + keysLength);
        });
    };
    Tab1Page = tslib_1.__decorate([
        Injectable(),
        Component({
            selector: 'app-tab1',
            templateUrl: 'tab1.page.html',
            styleUrls: ['tab1.page.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [Platform, ToastController,
            Events, NavController,
            Storage,
            AlertController])
    ], Tab1Page);
    return Tab1Page;
}());
export { Tab1Page };
//# sourceMappingURL=tab1.page.js.map