import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { NFC, Ndef } from '@ionic-native/nfc/ngx';
import { Events } from '@ionic/angular';
//import { Message } from '@angular/compiler/src/i18n/i18n_ast';
var Tab2Page = /** @class */ (function () {
    function Tab2Page(nfc, ndef, events) {
        var _this = this;
        this.nfc = nfc;
        this.ndef = ndef;
        this.events = events;
        this.buff = [];
        this.send = [];
        this.ndefMsg = [];
        this.readingTag = false;
        this.writingTag = false;
        this.isWriting = false;
        this.subscriptions = new Array();
        this.message = [];
        this.subscriptions.push(this.nfc.addNdefListener()
            .subscribe(function (data) {
            if (_this.readingTag) {
                var record = data.tag.ndefMessage.length;
                for (var i = 0; i < record; i++) {
                    var payload = data.tag.ndefMessage[i].payload;
                    _this.buff[i] = _this.nfc.bytesToString(payload).substring(3);
                }
                _this.panel1 = _this.buff[0];
                _this.panel2 = _this.buff[1];
                _this.panel3 = _this.buff[2];
                _this.panel4 = _this.buff[3];
                //this.panel4=JSON.stringify(data);
                _this.readingTag = false;
            }
            else if (_this.writingTag) {
                //this.nfc.write([this.ndefMsg[0]]);
                _this.nfc.write(_this.message);
                _this.writingTag = false;
                /* .then(() =>
                   {
                     this.writingTag = false;
                      
                   })
                   .catch(err => {
                     this.panel1='error writing';
                     this.writingTag = false;
                   });*/
                //}
            }
        }, function (err) {
            _this.panel1 = 'No Tag...';
        }));
    }
    Tab2Page.prototype.proses = function () {
        this.message[0] = this.ndef.textRecord(this.panel5);
        this.message[1] = this.ndef.textRecord(this.panel6);
        this.message[2] = this.ndef.textRecord(this.panel7);
        this.message[3] = this.ndef.textRecord(this.panel8);
        this.writingTag = true;
    };
    Tab2Page.prototype.tampil = function () {
        for (var i = 0; i < 4; i++) {
            this.buff[i] = "";
        }
        this.readTag();
    };
    Tab2Page.prototype.ionViewWillLeave = function () {
        this.subscriptions.forEach(function (sub) {
            sub.unsubscribe();
        });
    };
    Tab2Page.prototype.readTag = function () {
        this.readingTag = true;
    };
    Tab2Page.prototype.writeTag = function (writeText) {
        this.writingTag = true;
    };
    Tab2Page = tslib_1.__decorate([
        Component({
            selector: 'app-tab2',
            templateUrl: 'tab2.page.html',
            styleUrls: ['tab2.page.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [NFC,
            Ndef,
            Events])
    ], Tab2Page);
    return Tab2Page;
}());
export { Tab2Page };
//# sourceMappingURL=tab2.page.js.map