import { Component, OnInit } from '@angular/core';
import { Events, NavController } from '@ionic/angular';
import { ToastController,AlertController} from '@ionic/angular';
import { Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage';
//import { RestApiService } from '../rest-api.service';

declare var cordova: any;

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  public formattedDate:any;
  public fDate:any;
  public Time:any;

  public pTest:any;
  public pPit:any;
  public pSeam:any='';
  public pKodedriver:any='';
  public pDriver:any='';
  public pNobody:any='';
  public pNopol:any='';
  public pKontraktor:any='';
  public pChecker:any='';
  public Tiket:number=0;

  public trig:boolean=false;
  public checkQR:boolean;
  public rawData: any;

  public pEncrypt:any;
  public pDecrypt:any;
  public secureKey: any='1234567890abcdefghijklmnopqrstuv'; // Any string, the length should be 32
  public secureIV: any='1234567890abcdef'; // Any string, the length should be 16

  public data: any;
  public bool:boolean=false;

  public TriListPIT:boolean=false;
  public TriListSEAM:boolean=false;
  public TriListKDriver:boolean=false;
  public TriListDriver:boolean=false;
  public TriListNobody:boolean=false;
  public TriListNopol:boolean=false;
  public TriListKontraktor:boolean=false;
  public TriListChecker:boolean=false;

  public itemsPIT: any = ["Granit","Tanggo"];
  public itemsSEAM: any = ["Seam 4","Seam 5","Seam 6"];
  public itemsKDriver: any = ["001","002","003","004","005","006","007","008","009","010"];
  public itemsDriver: any = ["BRU HAROMA","BRU SURYADI","BRU DAVID.TJ","BRU SENEN","BRU PAHRI","BRU DONI T","PAHRI","CANDRA","BRU ABUROHIM","BRU BUDIMAN"];
  public itemsNobody: any = ["BG8557US","BG8520UQ","BG8836UQ","BG8658DC","BG8669DC","BG8467UQ","BG8655DC","BG8563US","BG8473UQ","BG8567US"];
  public itemsNopol: any = ["BG8557US","BG8520UQ","BG8836UQ","BG8658DC","BG8669DC","BG8467UQ","BG8655DC","BG8563US","BG8473UQ","BG8567US"];
  public itemsKontraktor: any = ["BRU","DS","STO","HM","SR","BBL","ANR","IND","SHR"];
  public itemsChecker: any = ["HAROMA","SURYADI","DAVID.TJ","SENEN","PAHRI","DONI T","PAHRI","CANDRA","ABUROHIM","BUDIMAN"];
  
  public count:number=0;

  constructor
  (
    private platform: Platform,
    private toastCtrl: ToastController,
    public events: Events, 
    public navCtrl: NavController,
    private storage: Storage,
    private alertCtrl: AlertController
    //public rest: RestApiService
    
  ) 
  {
    this.setValue("PIT", this.itemsPIT);
    this.setValue("SEAM", this.itemsSEAM);
    this.setValue("KDriver", this.itemsKDriver);
    this.setValue("Driver", this.itemsDriver);
    this.setValue("Nobody", this.itemsNobody);
    this.setValue("Nopol", this.itemsNopol);
    this.setValue("Kontraktor", this.itemsKontraktor);
    this.setValue("Checker", this.itemsChecker);

    this.getFormattedDate();
  }

  createUser() 
  {
    this.rawPrint();
    this.trig=true;
    this.events.publish('hello', this.rawData,this.trig);
    /*this.events.subscribe('hello', (this.sendData())=> {
      // user and time are the same arguments passed in `events.publish(user, time)`
      this.dataSend= panel11+" "+ time;
    });*/
  }

  rawPrint()
  {
    this.getFormattedDate();
    this.Tiket=this.Tiket+1;
    
    this.rawData  ='\x1B\x64\x01\x1B\x40\x1B\x74\x00\x1B\x61\x01\x1B\x21\x18DO Pengiriman Batubara\n'
    this.rawData +='PT BSPC\n\n';
    this.rawData +='\x1B\x61\x00\x1B\x21\x08No tiket   : '+this.Tiket+'\n';
    this.rawData +='\x1B\x61\x00\x1B\x21\x00PIT        : '+this.pPit+'\n';
    this.rawData +=                                            'SEAM       : '+this.pSeam+'\n';
    this.rawData +='\x1B\x61\x00\x1B\x21\x08Kode Driver: '+this.pKodedriver+'\n';
    this.rawData +=                                            'Driver     : '+this.pDriver+'\n';
    this.rawData +=                                            'No Body    : '+this.pNobody+'\n';
    this.rawData +='\x1B\x61\x00\x1B\x21\x00No Pol     : '+this.pNopol+'\n';
    this.rawData +=                                            'Kontraktor : '+this.pKontraktor+'\n';
    this.rawData +=                                            'Tgl        : '+this.formattedDate+'\n';
    this.rawData +=                                            'Jam        : '+this.Time+'\n\n\n\n\n';
    this.rawData +=                                            'Checker    : '+this.pChecker+'\n\n';
    //this.rawData +='QR code :';
    if(this.checkQR==true)
    {
      let text = this.Tiket+','+this.pPit+','+this.pSeam+','+this.pKodedriver+','+this.pNobody+','+this.pKontraktor+','+this.fDate+','+this.Time;
      //this.dataEncrypt=text;
      let dataq: any[150]={};
      let len= text.length+3; 
      
      dataq=['\x00','\x01','\x02','\x03','\x04','\x05','\x06','\x07','\x08','\x09','\x0A','\x0B','\x0C','\x0D','\x0E','\x0F','\x10','\x11','\x12','\x13','\x14',
             '\x15','\x16','\x17','\x18','\x19','\x1A','\x1B','\x1C','\x1D','\x1E','\x1F','\x20','\x21','\x22','\x23','\x24','\x25','\x26','\x27','\x28','\x29',
             '\x2A','\x2B','\x2C','\x2D','\x2E','\x2F','\x30','\x31','\x32','\x33','\x34','\x35','\x36','\x37','\x38','\x39','\x3A','\x3B','\x3C','\x3D','\x3E',
             '\x3F','\x40','\x41','\x42','\x43','\x44','\x45','\x46','\x47','\x48','\x49','\x4A','\x4B','\x4C','\x4D','\x4E','\x4F','\x50','\x51','\x52','\x53',
             '\x54','\x55','\x56','\x57','\x58','\x59','\x5A','\x5B','\x5C','\x5D','\x5E','\x5F','\x60','\x61','\x62','\x63','\x64','\x65','\x66','\x67','\x68',
             '\x69','\x6A','\x6B','\x6C','\x6D','\x6E','\x6F','\x70','\x71','\x72','\x73','\x74','\x75','\x76','\x77','\x78','\x79','\x7A','\x7B','\x7C','\x7D',
             '\x7E','\x7F','\x80','\x81','\x82','\x83','\x84','\x85','\x86','\x87','\x88','\x89','\x8A','\x8B','\x8C','\x8D','\x8E','\x8F','\x90','\x91','\x92'];
      
   
      this.rawData += '\x1B\x40\x1B\x74\x00\x1B\x40\x1B\x61\x01\x1D\x28\x6B\x04\x00\x31\x41\x32\x00\x1D\x28\x6B\x03\x00\x31\x43\x08\x1D\x28\x6B\x03\x00\x31\x45\x33\x1D\x28\x6B'+dataq[len]+'\x00\x31\x50\x30'+ text+'\x1D\x28\x6B\x03\x00\x31\x51\x30\x0D\x0D';
      this.rawData += '\x1B\x61\x01\x1B\x21\x00'+this.Tiket+' '; 
      this.rawData += this.formattedDate+' ';
      this.rawData += this.Time+'\n\n\n\n\n';
      
    }
    else
    {
      this.rawData +='\n\n\n';
    }


  }

  getFormattedDate()
  {
    var dataObj= new Date();

    var year=dataObj.getFullYear().toString();
    var month=dataObj.getMonth().toString();
    var date= dataObj.getDate().toString();

    var hours=dataObj.getHours().toString();
    var minute=dataObj.getMinutes().toString();
    var second=dataObj.getSeconds().toString();
  

    var montArray=['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];
    var minArray=['00','01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39','40','41','42','43','44','45','46','47','48','49','50','51','52','53','54','55','56','57','58','59'];
    var secArray=['00','01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39','40','41','42','43','44','45','46','47','48','49','50','51','52','53','54','55','56','57','58','59'];
    var mArray=['01','02','03','04','05','06','07','08','09','10','11','12'];

    this.formattedDate=date+' '+montArray[month]+' '+year;
    this.fDate=date+'-'+mArray[month]+'-'+year;
    this.Time=hours+':'+minArray[minute]+':'+secArray[second];
  }

  encrypt1()
  {

    this.encrypt(this.secureKey, this.secureIV, this.pEncrypt); 

  }
  encrypt(secureKey, secureIV, data) {
    this.platform.ready().then(() => {
      cordova.plugins.AES256.encrypt(secureKey, secureIV, data,
        (encrypedData) => {
          //this.pNobody='masuk pak';
          this.pEncrypt=encrypedData;
          this.showToast('Ok');
        }, (error) => {
          this.showToast(error);
        });
    });
  }

  decrypt1()
  {
    this.decrypt(this.secureKey, this.secureIV, this.pDecrypt);  

  }
  decrypt(secureKey, secureIV, encryptedData) {
    this.platform.ready().then(() => {
      cordova.plugins.AES256.decrypt(secureKey, secureIV, encryptedData,
        (decryptedData) => {
          //this.pDriver='masuk pak';
          this.pDecrypt=decryptedData;
          this.showToast('Ok');
        }, (error) => {
          this.showToast(error);
        });
    });
  }
  
  generateSecureKey(password) {
    this.platform.ready().then(() => {
      cordova.plugins.AES256.generateSecureKey(password,
        (secureKey) => {
          this.secureKey = secureKey;
          //this.showToast(secureKey);  
          //this.pTiket=secureKey +' '+password;        
        }, (error) => {
          this.showToast(error);
        });
    });
  }
  
  generateSecureIV(password) {
    this.platform.ready().then(() => {
      cordova.plugins.AES256.generateSecureIV(password,
        (secureIV) => {
          this.secureIV = secureIV;
          //this.showToast(secureIV);     
          this.pPit=secureIV+' '+password;         
        }, (error) => {
          this.showToast(error);
        });
    });
  }

  async showToast(msj) 
  {
    const toast = await this.toastCtrl.create(
    {
      message: msj,
      position: 'bottom',
      duration: 1000
    });
    return await toast.present();

  }

  async showError(error) 
  {
    let alert = await this.alertCtrl.create(
    {

    header: 'Error',
    //subHeader: 'error',
    message: error,
    buttons: ['Dismiss']

    });
    return await alert.present();
  }

    // set a key/value
    setValue(key: string, value: any) 
    {
      
      this.storage.set(key, value).then((response) => 
      {
        //this.showToast('set' + key + ' '+response);
   
      })
      .catch((error) => {
        //this.showToast('set error for ' + key + ' '+error);
      });
    }
   
    // get a key/value pair
    getValue(key: string) 
    {
      this.storage.get(key).then((val) => 
      {
        //value=val;
        //return value;
        //this.itemsPIT=val;
      }).catch((error) => {
        this.showToast('get error for ' + key + ' '+error);
      });
    }
   
    // Remove a key/value pair
    removeKey(key: string) 
    {
      this.storage.remove(key).then(() => {
        this.showToast('removed ' + key);
        this.data[key] = "";
      }).catch((error) => {
        this.showToast('removed error for ' + key + ''+error);
      });
    }
   
    //Get Current Storage Engine Used
    driverUsed() 
    {
      this.showToast("Driver Used: " + this.storage.driver);
    }
   
    // Traverse key/value pairs
    traverseKeys() 
    {
      this.storage.forEach((value: any, key: string, iterationNumber: Number) => 
      {
        this.pTest="key=" + key +" iterationNumber=" + iterationNumber+" value=" + value;
      });
    }
    // Traverse key/value pairs
    listKeys() 
    {
      this.storage.keys().then((k) => 
      {
        this.showToast(k[0]+'\n'+k[1]+'\n'+k[2]+'\n'+k[3]+'\n'+k[4]);

      });
    }
    // Total Keys Stored
    getKeyLength() 
    {
      this.storage.length().then((keysLength: Number) => 
      {
        this.showToast("Total Keys " + keysLength);
      });
    }
   

    /*filterItems(searchTerm) 
    {
      return this.itemsPIT.filter(item =>
      {
        return item.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
      });
    }*/


    setFilteredItemsPIT() 
    {
      if(this.bool==false)
      {
        this.TriListPIT=true;
        //this.itemsPIT = this.filterItems(this.pPit);
        this.itemsPIT=this.itemsPIT.filter(item =>
          {
            return item.toLowerCase().indexOf(this.pPit.toLowerCase()) > -1;
          });
        
        if(this.pPit=="")
        {
          //this.itemsPIT=this.getValue("PIT");
          this.storage.get("PIT").then((val) => 
          {this.itemsPIT=val;})
          .catch((error) => {this.showToast('get error for ' + 'PIT' + ' '+error);});
        }
      
        //this.showToast("this item= "+this.itemsPIT);

      }

      if(this.pPit== (this.itemsPIT = this.itemsPIT.filter(item =>
        {
          return item.toLowerCase().indexOf(this.pPit.toLowerCase()) > -1;
        })))

      {
        this.pitList(this.pPit);
      }

      else
      {
        this.bool=false;
      }

    }

    pitList(msg)
    {
      this.TriListPIT=false;
      this.pPit=msg;
      this.bool=true;   
    }


    setFilteredItemsSEAM() 
    {
      if(this.bool==false)
      {
        this.TriListSEAM=true;
        //this.itemsPIT = this.filterItems(this.pPit);
        this.itemsSEAM=this.itemsSEAM.filter(item =>
          {
            return item.toLowerCase().indexOf(this.pSeam.toLowerCase()) > -1;
          });
        
        if(this.pSeam=="")
        {
          this.storage.get("SEAM").then((val) => 
          {this.itemsSEAM=val;})
          .catch((error) => {this.showToast('get error for ' + 'SEAM' + ' '+error);});
        }
      
      }

      if(this.pSeam== (this.itemsSEAM = this.itemsSEAM.filter(item =>
        {
          return item.toLowerCase().indexOf(this.pSeam.toLowerCase()) > -1;
        })))

      {
        this.seamList(this.pSeam);
      }

      else
      {
        this.bool=false;
      }

    }

    seamList(msg)
    {
      this.TriListSEAM=false;
      this.pSeam=msg;
      this.bool=true;   
    }

    setFilteredItemsKDriver() 
    {

      if(this.bool==false)
      {
        this.TriListKDriver=true;
        //this.itemsPIT = this.filterItems(this.pPit);
        this.itemsKDriver=this.itemsKDriver.filter(item =>
          {
            return item.toLowerCase().indexOf(this.pKodedriver.toLowerCase()) > -1;
          });   

          if(this.pKodedriver=="")
          {
            //this.TriListKDriver=true;
            this.storage.get("KDriver").then((val) => 
            {this.itemsKDriver=val;})
            .catch((error) => {this.showToast('get error for ' + 'KDriver' + ' '+error);});
          }
      }
      
      if(this.pKodedriver== (this.itemsKDriver= this.itemsKDriver.filter(item =>
        {
          return item.toLowerCase().indexOf(this.pKodedriver.toLowerCase()) > -1;
        })))

      {
        this.kdriverList(this.pKodedriver);
      }

      else
      {
        this.bool=false;
      }

    }

    kdriverList(msg)
    {
      this.TriListKDriver=false;
      this.pKodedriver=msg;
      this.bool=true;   
    }

    setFilteredItemsDriver() 
    {

      if(this.bool==false)
      {
        this.TriListDriver=true;
        //this.itemsPIT = this.filterItems(this.pPit);
        this.itemsDriver=this.itemsDriver.filter(item =>
          {
            return item.toLowerCase().indexOf(this.pDriver.toLowerCase()) > -1;
          });   

          if(this.pDriver=="")
          {
            //this.TriListKDriver=true;
            this.storage.get("Driver").then((val) => 
            {this.itemsDriver=val;})
            .catch((error) => {this.showToast('get error for ' + 'Driver' + ' '+error);});
          }
      }
      
      if(this.pDriver== (this.itemsDriver= this.itemsDriver.filter(item =>
        {
          return item.toLowerCase().indexOf(this.pDriver.toLowerCase()) > -1;
        })))

      {
        this.DriverList(this.pDriver);
      }

      else
      {
        this.bool=false;
      }

    }

    DriverList(msg)
    {
      this.TriListDriver=false;
      this.pDriver=msg;
      this.bool=true;   
    }

    setFilteredItemsNobody() 
    {

      if(this.bool==false)
      {
        this.TriListNobody=true;
        //this.itemsPIT = this.filterItems(this.pPit);
        this.itemsNobody=this.itemsNobody.filter(item =>
          {
            return item.toLowerCase().indexOf(this.pNobody.toLowerCase()) > -1;
          });   

          if(this.pNobody=="")
          {
            //this.TriListKDriver=true;
            this.storage.get("Nobody").then((val) => 
            {this.itemsNobody=val;})
            .catch((error) => {this.showToast('get error for ' + 'Nobody' + ' '+error);});
          }
      }
      
      if(this.pNobody== (this.itemsNobody= this.itemsNobody.filter(item =>
        {
          return item.toLowerCase().indexOf(this.pNobody.toLowerCase()) > -1;
        })))

      {
        this.NobodyList(this.pNobody);
      }

      else
      {
        this.bool=false;
      }

    }

    NobodyList(msg)
    {
      this.TriListNobody=false;
      this.pNobody=msg;
      this.bool=true;   
    }

    setFilteredItemsNopol() 
    {

      if(this.bool==false)
      {
        this.TriListNopol=true;
        //this.itemsPIT = this.filterItems(this.pPit);
        this.itemsNopol=this.itemsNopol.filter(item =>
          {
            return item.toLowerCase().indexOf(this.pNopol.toLowerCase()) > -1;
          });   

          if(this.pNopol=="")
          {
            //this.TriListKDriver=true;
            this.storage.get("Nopol").then((val) => 
            {this.itemsNopol=val;})
            .catch((error) => {this.showToast('get error for ' + 'Nopol' + ' '+error);});
          }
      }
      
      if(this.pNopol== (this.itemsNopol= this.itemsNopol.filter(item =>
        {
          return item.toLowerCase().indexOf(this.pNopol.toLowerCase()) > -1;
        })))

      {
        this.NopolList(this.pNopol);
      }

      else
      {
        this.bool=false;
      }

    }

    NopolList(msg)
    {
      this.TriListNopol=false;
      this.pNopol=msg;
      this.bool=true;   
    }

    setFilteredItemsKontraktor() 
    {

      if(this.bool==false)
      {
        this.TriListKontraktor=true;
        //this.itemsPIT = this.filterItems(this.pPit);
        this.itemsKontraktor=this.itemsKontraktor.filter(item =>
          {
            return item.toLowerCase().indexOf(this.pKontraktor.toLowerCase()) > -1;
          });   

          if(this.pKontraktor=="")
          {
            //this.TriListKDriver=true;
            this.storage.get("Kontraktor").then((val) => 
            {this.itemsKontraktor=val;})
            .catch((error) => {this.showToast('get error for ' + 'Kontraktor' + ' '+error);});
          }
      }
      
      if(this.pKontraktor== (this.itemsKontraktor= this.itemsKontraktor.filter(item =>
        {
          return item.toLowerCase().indexOf(this.pKontraktor.toLowerCase()) > -1;
        })))

      {
        this.KontraktorList(this.pKontraktor);
      }

      else
      {
        this.bool=false;
      }

    }

    KontraktorList(msg)
    {
      this.TriListKontraktor=false;
      this.pKontraktor=msg;
      this.bool=true;   
    }

    setFilteredItemsChecker() 
    {

      if(this.bool==false)
      {
        this.TriListChecker=true;
        //this.itemsPIT = this.filterItems(this.pPit);
        this.itemsChecker=this.itemsChecker.filter(item =>
          {
            return item.toLowerCase().indexOf(this.pChecker.toLowerCase()) > -1;
          });   

          if(this.pChecker=="")
          {
            //this.TriListKDriver=true;
            this.storage.get("Checker").then((val) => 
            {this.itemsChecker=val;})
            .catch((error) => {this.showToast('get error for ' + 'Checker' + ' '+error);});
          }
      }
      
      if(this.pChecker== (this.itemsChecker= this.itemsChecker.filter(item =>
        {
          return item.toLowerCase().indexOf(this.pChecker.toLowerCase()) > -1;
        })))

      {
        this.CheckerList(this.pChecker);
      }

      else
      {
        this.bool=false;
      }

    }

    CheckerList(msg)
    {
      this.TriListChecker=false;
      this.pChecker=msg;
      this.bool=true;   
    }
    
}
