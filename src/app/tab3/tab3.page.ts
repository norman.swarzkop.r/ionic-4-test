import { Component } from '@angular/core';
import { BluetoothSerial } from '@ionic-native/bluetooth-serial/ngx';
import { NavController } from '@ionic/angular';
import { AlertController, ToastController } from '@ionic/angular';
import { BarcodeScanner, BarcodeScannerOptions } from '@ionic-native/barcode-scanner/ngx';
import {Platform, Events} from '@ionic/angular';

@Component(
{
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss'],
  
})
export class Tab3Page 
{
  //j: number;
  pairedDeviceID: number=0;
  pairedList: pairedlist;
  listToggle: boolean = false;
  listConn: boolean = false;
  listDisConn: boolean = false;
  
  dataSend: string = "";
  QRText: string="";
  
  
  options: BarcodeScannerOptions;
  encodeText: string=''; 
  encodeData: any={};
  scannedData: any={};
  
  constructor 
  (
    private bluetoothSerial: BluetoothSerial,
    public navCtrl: NavController,
    private alertCtrl: AlertController, 
    private toastCtrl: ToastController,
    public scanner: BarcodeScanner,
    public platform: Platform,
    public events: Events
  )

  {

    //this.events.publish('hello', this.sendData());
    this.events.subscribe('hello', (dataBuff, trig)=> 
    {
      if(trig==true)
      {

        this.sendRaw(dataBuff);

      }
    });
    this.checkBluetoothEnabled();
  }

  exitApp()
  {
    //this.platform.
    //this.platform.navigator[‘app’].exitApp()
    //cordova.plugins.exit();
    navigator['app'].exitApp();
  }

  checkBluetoothEnabled() 
  {
    this.bluetoothSerial.isEnabled().then(success => 
    {
      this.listPairedDevices();
    }, 
    error => 
    {
      this.showError("Please Enable Bluetooth")
    });
  }
 
  listPairedDevices() 
  {
    this.bluetoothSerial.list().then(success => {
      this.pairedList = success;
      this.listToggle = true;
      this.listConn = true;
      this.listDisConn = false;
      //this.encodeText='ok=  '+this.pairedList[this.index]+' '+ this.pairedDeviceID.length;
    }, error => {
      this.showError("Please Enable Bluetooth")
      this.listToggle = false;
      this.listConn = false;
      this.listDisConn = false;
    });
  }

  selectDevice() 
  {
    let connectedDevice = this.pairedList[this.pairedDeviceID];
    if (!connectedDevice.address) 
    {
      this.showError('Select Paired Device to connect');
      return;
    }
    let address = connectedDevice.address;
    let name = connectedDevice.name;
    //this.events.publish('hello', this.connect(address));
    this.connect(address);
  }

  connect(address) 
  {
    this.bluetoothSerial.connect(address).subscribe(success => {
      this.deviceConnected();
      this.showToast("Successfully Connected");
      this.listConn = false;
      this.listDisConn = true;
    }, error => {
      this.showError("Error:Connecting to Device");
      this.listConn = true;
      this.listDisConn = false;
    });
  }

  deviceConnected() 
  {
    this.bluetoothSerial.subscribe('\n').subscribe(success => {
      this.handleData(success);
      this.showToast("Connected Successfullly");
    }, error => {
      this.showError(error);
    });
  }

  deviceDisconnected() 
  {
    this.bluetoothSerial.disconnect();
    this.showToast("Device Disconnected");
    this.listConn = true;
    this.listDisConn = false;
  }

  handleData(data) 
  {
    this.showToast(data);
  }

  sendRaw(raw)
  {
    if(this.listDisConn==true)
    {
      //this.dataSend+='\n';
      //this.showToast(this.dataSend);
      this.bluetoothSerial.write(raw).then(success => {
      this.showToast(success);
      //this.dataSend='';
      
    }, error => {
      this.showError(error)
      //this.dataSend='';
    });
    }
    else
    {
      this.showToast("Device Disconnected"); 
    }

  }
  sendData() 
  {
    if(this.listDisConn==true)
    {
      this.dataSend+='\n';
      this.showToast(this.dataSend);
      this.bluetoothSerial.write(this.dataSend).then(success => {
      this.showToast(success);
      this.dataSend='';
      
    }, error => {
      this.showError(error)
      this.dataSend='';
    });
    }
    else
    {
      this.showToast("Device Disconnected"); 
    }
  }

  
printQR() 
{
    if(this.listDisConn==true)
    {
      let text = this.QRText;
      let dataq: any[150]={};
      let len= this.QRText.length+3; 
      
      dataq=['\x00','\x01','\x02','\x03','\x04','\x05','\x06','\x07','\x08','\x09','\x0A','\x0B','\x0C','\x0D','\x0E','\x0F','\x10','\x11','\x12','\x13','\x14',
             '\x15','\x16','\x17','\x18','\x19','\x1A','\x1B','\x1C','\x1D','\x1E','\x1F','\x20','\x21','\x22','\x23','\x24','\x25','\x26','\x27','\x28','\x29',
             '\x2A','\x2B','\x2C','\x2D','\x2E','\x2F','\x30','\x31','\x32','\x33','\x34','\x35','\x36','\x37','\x38','\x39','\x3A','\x3B','\x3C','\x3D','\x3E',
             '\x3F','\x40','\x41','\x42','\x43','\x44','\x45','\x46','\x47','\x48','\x49','\x4A','\x4B','\x4C','\x4D','\x4E','\x4F','\x50','\x51','\x52','\x53',
             '\x54','\x55','\x56','\x57','\x58','\x59','\x5A','\x5B','\x5C','\x5D','\x5E','\x5F','\x60','\x61','\x62','\x63','\x64','\x65','\x66','\x67','\x68',
             '\x69','\x6A','\x6B','\x6C','\x6D','\x6E','\x6F','\x70','\x71','\x72','\x73','\x74','\x75','\x76','\x77','\x78','\x79','\x7A','\x7B','\x7C','\x7D',
             '\x7E','\x7F','\x80','\x81','\x82','\x83','\x84','\x85','\x86','\x87','\x88','\x89','\x8A','\x8B','\x8C','\x8D','\x8E','\x8F','\x90','\x91','\x92'];
      
   
      this.bluetoothSerial.write('\x1B\x40\x1B\x74\x00\x1B\x40\x1B\x61\x01\x1D\x28\x6B\x04\x00\x31\x41\x32\x00\x1D\x28\x6B\x03\x00\x31\x43\x08\x1D\x28\x6B\x03\x00\x31\x45\x33\x1D\x28\x6B'+dataq[len]+'\x00\x31\x50\x30'+ text+'\x1D\x28\x6B\x03\x00\x31\x51\x30\x0D\x0D')
      .then(success => 
      {
        this.showToast(success);
        this.QRText='';
      }, 
      error => 
      {
        this.showError(error)
        this.QRText='';
      });
    }

    else
    {
      this.showToast("Device Disconnected"); 
    }
  }

QRprint() 
{
    if(this.listDisConn==true)
    {
      let text = this.QRText;
      let dataq: any[150]={};
      let len= this.QRText.length+3; 
      
      dataq=['\x00','\x01','\x02','\x03','\x04','\x05','\x06','\x07','\x08','\x09','\x0A','\x0B','\x0C','\x0D','\x0E','\x0F','\x10','\x11','\x12','\x13','\x14',
             '\x15','\x16','\x17','\x18','\x19','\x1A','\x1B','\x1C','\x1D','\x1E','\x1F','\x20','\x21','\x22','\x23','\x24','\x25','\x26','\x27','\x28','\x29',
             '\x2A','\x2B','\x2C','\x2D','\x2E','\x2F','\x30','\x31','\x32','\x33','\x34','\x35','\x36','\x37','\x38','\x39','\x3A','\x3B','\x3C','\x3D','\x3E',
             '\x3F','\x40','\x41','\x42','\x43','\x44','\x45','\x46','\x47','\x48','\x49','\x4A','\x4B','\x4C','\x4D','\x4E','\x4F','\x50','\x51','\x52','\x53',
             '\x54','\x55','\x56','\x57','\x58','\x59','\x5A','\x5B','\x5C','\x5D','\x5E','\x5F','\x60','\x61','\x62','\x63','\x64','\x65','\x66','\x67','\x68',
             '\x69','\x6A','\x6B','\x6C','\x6D','\x6E','\x6F','\x70','\x71','\x72','\x73','\x74','\x75','\x76','\x77','\x78','\x79','\x7A','\x7B','\x7C','\x7D',
             '\x7E','\x7F','\x80','\x81','\x82','\x83','\x84','\x85','\x86','\x87','\x88','\x89','\x8A','\x8B','\x8C','\x8D','\x8E','\x8F','\x90','\x91','\x92'];
      
   
      this.bluetoothSerial.write('\x1B\x40\x1B\x74\x00\x1B\x40\x1B\x61\x01\x1D\x28\x6B\x04\x00\x31\x41\x32\x00\x1D\x28\x6B\x03\x00\x31\x43\x08\x1D\x28\x6B\x03\x00\x31\x45\x33\x1D\x28\x6B'+dataq[len]+'\x00\x31\x50\x30'+ text+'\x1D\x28\x6B\x03\x00\x31\x51\x30\x0D\x0D')
      .then(success => 
      {
        this.showToast(success);
        this.QRText='';
      }, 
      error => 
      {
        this.showError(error)
        this.QRText='';
      });
    }

    else
    {
      this.showToast("Device Disconnected"); 
    }
  }


  async showError(error) 
  {
    let alert = await this.alertCtrl.create(
    {

    header: 'Error',
    //subHeader: 'error',
    message: error,
    buttons: ['Dismiss']

    });
    return await alert.present();
  }

  async showToast(msj) 
  {
    const toast = await this.toastCtrl.create(
    {
      message: msj,
      position: 'bottom',
      duration: 1000
    });
    return await toast.present();

  }
    

scan()
{
  this.options={
    prompt:'Scan you barcode'
  };
  this.scanner.scan(this.options).then((data)=>{
    this.scannedData=data;
  },(err)=>{
    this.showError('Error');
  })
}

encode(){
  this.scanner.encode(this.scanner.Encode.TEXT_TYPE,this.encodeText).then((data)=>{
    this.encodeData=data;
    this.dataSend=data;
    this.bluetoothSerial.write(data);
    //this.dataSend=JSON.stringify(data);
  },(err)=>{
    this.showToast(err);
  })

  
}


}

interface pairedlist 
{
  "class": number,
  "id": string,
  "address": string,
  "name": string
}
